# 机械工业出版社_《软件测试的艺术（原书第3版）》（张晓明）

## Chapter 02

1. P7–2.2.1黑盒测试–3级标题下第1段第2行
    - 译本翻译：**~~测试目标与程序的内部机制和结构完全无关~~**
    - 原著英文：Your goal is to be completely unconcerned about the internal behavior and structure of the program
    - 建议翻译：**测试目标完全不依赖于程序的内部行为和内部结构**
    - 建议原因：译本翻译容易引起误解成"测试目标与程序的内部机制 和结构完全无关"（"机制"之后断句），即将"测试目标"、"程序的内部机制"并列
